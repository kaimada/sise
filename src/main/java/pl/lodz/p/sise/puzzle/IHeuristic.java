/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.sise.puzzle;

/**
 *
 * @author michal
 */
public interface IHeuristic {
    int calculateCost(PuzzleBoard board);
}
