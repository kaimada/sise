/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.sise.puzzle;

/**
 *
 * @author michal
 */
public class Manhattan implements IHeuristic{

    @Override
    public int calculateCost(PuzzleBoard board) {
        int count = 0;
        int expected = 0;
        PuzzleBoard solvedBoard = board.getSolvedPuzzleBoard();
        for (int i = 0; i < board.getRows(); i++) {

            for (int j = 0; j < board.getColumns(); j++) {

                int value = board.getBoard()[i][j];
                expected++;

                if (value != 0 && value != expected) {
                    // Manhattan distance is the sum of the absolute values of
                    // the horizontal and the vertical distance
                    count += Math.abs(i
                            - solvedBoard.getRowOfSpecifiedValue(value))
                            + Math.abs(j
                                    -solvedBoard.getColumnOfSpecifiedValue(value));
                }
            }
        }

        return count;
    }
    
}
