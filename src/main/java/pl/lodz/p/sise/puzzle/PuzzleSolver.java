/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.sise.puzzle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 *
 * @author michal
 */
public class PuzzleSolver {
    
    private Node solvedNode;
    private int visitedNodesCounter;
    
    public PuzzleBoard solveWithBfs(PuzzleBoard initailBoard){
        Queue<Node> nodesToSearch = new LinkedList<>();
        HashSet<Node> visitedNodes = new HashSet<>();
        
        //ArrayList<Node> visitedNodes =new ArrayList<>();

        
        Node root=new Node(null,initailBoard);
        nodesToSearch.add(root);
        //visitedNodes.add(root);
        boolean isSolved=false;
        
        while(!nodesToSearch.isEmpty() && !isSolved){
            Node node = nodesToSearch.remove();
            
            if(node.getPuzzleBoard().isSolved()){
                solvedNode=node;
                isSolved=true;
                this.visitedNodesCounter=visitedNodes.size();
                break;
            }
            
            
            if(visitedNodes.contains(node))
                continue;
            ArrayList<PuzzleBoard> possibleMoves=node.getPuzzleBoard().generatePossibleMoves();
            for(PuzzleBoard possibleMove : possibleMoves){
                Node tempNode=new Node(node,possibleMove);
                if(!visitedNodes.contains(tempNode))
                   nodesToSearch.add(tempNode);  
            visitedNodes.add(node);

        }
        }
        return solvedNode.getPuzzleBoard();
        
    
    }
    
    public PuzzleBoard solveWithAStar(PuzzleBoard board, IHeuristic heuristic){
        PriorityQueue<NodeWithCost> openNodes = new PriorityQueue<>();
        
        HashSet<NodeWithCost> visitedNodes=new HashSet<>();

        NodeWithCost currentNode=null;
        NodeWithCost root = new NodeWithCost(null,board,0);
        openNodes.add(root);
        boolean isSolved =false;

        
        while(!isSolved && !openNodes.isEmpty()){
            currentNode=openNodes.remove();
            
            if(visitedNodes.contains(currentNode))
                continue;
            if(currentNode.getPuzzleBoard().isSolved()){
                solvedNode=currentNode;
                isSolved=true;
                this.visitedNodesCounter=visitedNodes.size();
                break;
            }
            
            ArrayList<PuzzleBoard> possibleMoves=currentNode.getPuzzleBoard().generatePossibleMoves();
            ArrayList<Integer> costs =new ArrayList<>();
            
            for(PuzzleBoard possibleMove : possibleMoves){
                costs.add(currentNode.getCurrentCost()+heuristic.calculateCost(possibleMove));
            }
                
            for(int i=0;i<possibleMoves.size();i++){
                openNodes.add(new NodeWithCost(currentNode,possibleMoves.get(i),costs.get(i)));
            }
                
            visitedNodes.add(currentNode);
                
            }
        
        return solvedNode.getPuzzleBoard();
    }
    
    private int calculateIndexWithMinValue(ArrayList<Integer> values){
        if(values.size()==0)
            return -1;
        int minIndex=0;
        int minValue=values.get(0);
        for(int i=0;i<values.size();i++)
            if(values.get(i)<minValue)
                minIndex=i;
        return minIndex;
    }
    
    public String createSolutionPath(){
        
        if(solvedNode == null)
            return null;
        else{
            //int moveCounter = 0;
            String outcome ="";
            List<PuzzleBoard> boardList = new ArrayList();
            Node tempNode = solvedNode;
            while(tempNode.getParent() != null){
                boardList.add(tempNode.getPuzzleBoard());
                tempNode = tempNode.getParent();
                //moveCounter++;
            }
            Collections.reverse(boardList);
            for(PuzzleBoard board : boardList){
                outcome += board.boardToString();
                outcome += "\n----------------------------------------";
            }
            
            outcome += "\nIlość ruchów : " + (boardList.size()-1);
            outcome += "\nIlość odwiedzonych węzłów : " + visitedNodesCounter;
            return outcome;
            }
    }

        
    
    
}
