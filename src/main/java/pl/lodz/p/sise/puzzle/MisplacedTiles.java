/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.sise.puzzle;

/**
 *
 * @author michal
 */
public class MisplacedTiles implements IHeuristic{

    @Override
    public int calculateCost(PuzzleBoard board) {
        int cost = 0;
        
        int counter=1;
        
        for(int i=0;i<board.getRows();i++)
            for(int j=0;j<board.getColumns();j++){
                if(i == board.getRows()-1 && j == board.getColumns()-1)
                    counter=0;
                if(board.getBoard()[i][j]!=counter)
                    cost+=1;
                counter++;
            }
        return cost;
    }
    
}
