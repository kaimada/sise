/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.lodz.p.sise.puzzle;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author michal
 */
public class PuzzleLoader {
    public static PuzzleBoard LoadPuzzle(File file) throws FileNotFoundException{
        Scanner scanner=new Scanner(file);
        int rows = Integer.parseInt(scanner.nextLine());
        int columns = Integer.parseInt(scanner.nextLine());
        String numbers = scanner.nextLine();
        String[] numbersTab=numbers.split(" ");
        int counter=0;
        int[][] numbersTabInt = new int[rows][columns];
            for(int i=0;i<rows;i++)
                for(int j=0;j<columns;j++){
                    numbersTabInt[i][j]=Integer.parseInt(numbersTab[counter]);
                    counter++;
                }
        PuzzleBoard board = new PuzzleBoard(rows, columns);
        board.initializeBoardWithValues(numbersTabInt);
        return board;
            
    }
    
}
